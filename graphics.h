// $Id: graphics.h,v 1.1 2015-07-16 16:47:51-07 - - $

/**
 * Edited by Arnaldo Carneiro
 * 1506372
 * ardasilv@ucsc.edu
 */

#ifndef __GRAPHICS_H__
#define __GRAPHICS_H__

#include <memory>
#include <vector>
using namespace std;

#include <GL/freeglut.h>

#include "rgbcolor.h"
#include "shape.h"

class object {
   private:
      shared_ptr<shape> pshape;
      vertex center;
      rgbcolor color;
   public:
      // Default copiers, movers, dtor all OK.
      object(shape_ptr shape, vertex center, rgbcolor color):
          pshape(shape), center(center), color(color) {}
      void draw() { pshape->draw (center, color); }
      void move (GLfloat delta_x, GLfloat delta_y) {
         center.xpos += delta_x;
         center.ypos += delta_y;
      }
};

class mouse {
      friend class window;
   private:
      int xpos {0};
      int ypos {0};
      int entered {GLUT_LEFT};
      int left_state {GLUT_UP};
      int middle_state {GLUT_UP};
      int right_state {GLUT_UP};
   private:
      void set (int x, int y) { xpos = x; ypos = y; }
      void state (int button, int state);
      void draw();
};


class window {
      friend class mouse;
   private:
      static int width;         // in pixels
      static int height;        // in pixels
      static vector<object> objects;
      static size_t selected_obj;
      static GLfloat move_by;
      static GLfloat border_width;
      static rgbcolor border_color;
      static mouse mus;
      static bool draw_border;
   private:
      static void close();
      static void entry (int mouse_entered);
      static void display();
      static void reshape (int width, int height);
      static void keyboard (GLubyte key, int, int);
      static void special (int key, int, int);
      static void motion (int x, int y);
      static void passive_motion (int x, int y);
      static void mousefn (int button, int state, int x, int y);
   public:
      static void push_back (const object& obj) {
                  objects.push_back (obj); }
      static void set_width (int width_) { width = width_; }
      static void set_height (int height_) { height = height_; }
      static void set_border (GLfloat border_width_,
                             rgbcolor border_color_) {
                  border_width = border_width_;
                  border_color = border_color_; }
      static void set_movement_rate (GLfloat move_by_) {
                  move_by = move_by_; }
      static GLfloat get_border_width () { return border_width; }
      static rgbcolor get_border_color () { return border_color; }
      static void select_object (size_t object_number);
      static void set_draw_border() { draw_border = true; }
      static void reset_draw_border() { draw_border = false; }
      static bool has_to_draw_border() { return draw_border; }
      static void main();
};

#endif
