// $Id: shape.cpp,v 1.1 2015-07-16 16:47:51-07 - - $

/**
 * Edited by Arnaldo Carneiro
 * 1506372
 * ardasilv@ucsc.edu
 */

#include <typeinfo>
#include <unordered_map>
#include <cmath>
using namespace std;

#include "shape.h"
#include "util.h"
#include "graphics.h"

static unordered_map<void*,string> fontname {
   {GLUT_BITMAP_8_BY_13       , "Fixed-8x13"    },
   {GLUT_BITMAP_9_BY_15       , "Fixed-9x15"    },
   {GLUT_BITMAP_HELVETICA_10  , "Helvetica-10"  },
   {GLUT_BITMAP_HELVETICA_12  , "Helvetica-12"  },
   {GLUT_BITMAP_HELVETICA_18  , "Helvetica-18"  },
   {GLUT_BITMAP_TIMES_ROMAN_10, "Times-Roman-10"},
   {GLUT_BITMAP_TIMES_ROMAN_24, "Times-Roman-24"},
};

static unordered_map<string,void*> fontcode {
   {"Fixed-8x13"    , GLUT_BITMAP_8_BY_13       },
   {"Fixed-9x15"    , GLUT_BITMAP_9_BY_15       },
   {"Helvetica-10"  , GLUT_BITMAP_HELVETICA_10  },
   {"Helvetica-12"  , GLUT_BITMAP_HELVETICA_12  },
   {"Helvetica-18"  , GLUT_BITMAP_HELVETICA_18  },
   {"Times-Roman-10", GLUT_BITMAP_TIMES_ROMAN_10},
   {"Times-Roman-24", GLUT_BITMAP_TIMES_ROMAN_24},
};

ostream& operator<< (ostream& out, const vertex& where) {
   out << "(" << where.xpos << "," << where.ypos << ")";
   return out;
}

shape::shape() {
   DEBUGF ('c', this);
}

text::text (void* glut_bitmap_font, const string& textdata):
      glut_bitmap_font(glut_bitmap_font), textdata(textdata) {
   DEBUGF ('c', this);
}

ellipse::ellipse (GLfloat width, GLfloat height):
dimension ({width, height}) {
   DEBUGF ('c', this);
}

circle::circle (GLfloat diameter): ellipse (diameter, diameter) {
   DEBUGF ('c', this);
}


polygon::polygon (const vertex_list& vertices): vertices(vertices) {
   DEBUGF ('c', this);
}

rectangle::rectangle (GLfloat width, GLfloat height):
            polygon({{-width/2, -height/2}, {width/2, -height/2},
            {width/2, height/2}, {-width/2, height/2}}) {
   DEBUGF ('c', this << "(" << width << "," << height << ")");
}

square::square (GLfloat width): rectangle (width, width) {
   DEBUGF ('c', this);
}

diamond::diamond(const GLfloat width, const GLfloat height):
            polygon({{0, -height/2}, {width/2, 0},
            {0, height/2}, {-width/2, 0}}) {
   DEBUGF ('c', this << "(" << width << "," << height << ")");
}
            
triangle::triangle(const vertex& v1,
                   const vertex& v2,
                   const vertex& v3):
            polygon({{v1.xpos, v1.ypos},
                     {v2.xpos, v2.ypos},
                     {v3.xpos, v3.ypos}}) {
   DEBUGF ('c', this << "(" << v1 << "," << v2 << "," << v3 << ")");
}
equilateral::equilateral(const GLfloat side):
            triangle({-side/2, -(GLfloat) (side/4*sqrt(3))},
                     {side/2, -(GLfloat) (side/4*sqrt(3))},
                     {0, (GLfloat) (side/4*sqrt(3))}) {
}

void text::draw (const vertex& center, const rgbcolor& color) const {
   DEBUGF ('d', this << "(" << center << "," << color << ")");
   auto message =
         reinterpret_cast<const GLubyte*> (this->textdata.c_str());
   void* font {this->glut_bitmap_font};
   int str_width = glutBitmapLength (font, message);
   int str_height = glutBitmapHeight (font);
   glColor3ubv(color.ubvec);
   float xpos = center.xpos - str_width/2;
   float ypos = center.ypos - str_height/4;
   glRasterPos2f (xpos, ypos);
   for (auto ch: textdata) {
      int ch_width = glutBitmapWidth (font, ch);
      glutBitmapCharacter(font, ch);
      xpos += ch_width;
      glRasterPos2f(xpos, ypos);
   }
}

void ellipse::draw (const vertex& center, const rgbcolor& color) const {
   DEBUGF ('d', this << "(" << center << "," << color << ")");
   glEnable (GL_LINE_SMOOTH);
   glBegin (GL_POLYGON);
   glColor3ubv (color.ubvec);
   const float delta = 2 * M_PI / 64;
   for (float theta = 0; theta < 2 * M_PI; theta += delta) {
      float xpos = dimension.xpos * cos (theta) + center.xpos;
      float ypos = dimension.ypos * sin (theta) + center.ypos;
      glVertex2f (xpos, ypos);
   }
   glEnd();
   if (window::get_border_width() > 0 and
       window::has_to_draw_border()) {
      glLineWidth (window::get_border_width());
      glBegin (GL_LINE_LOOP);
      glColor3ubv (window::get_border_color().ubvec);
      for (float theta = 0; theta < 2 * M_PI; theta += delta) {
         float xpos = dimension.xpos * cos (theta) + center.xpos;
         float ypos = dimension.ypos * sin (theta) + center.ypos;
         glVertex2f (xpos, ypos);
      }
      glEnd();
   }
}

void polygon::draw (const vertex& center, const rgbcolor& color) const {
   DEBUGF ('d', this << "(" << center << "," << color << ")");
   glBegin (GL_POLYGON);
   glColor3ubv (color.ubvec);
   for (auto v: this->vertices) {
      glVertex2f (center.xpos + v.xpos, center.ypos + v.ypos);
   }
   glEnd();
   if (window::get_border_width() > 0 and
       window::has_to_draw_border()) {
      glLineWidth (window::get_border_width());
      glBegin (GL_LINE_LOOP);
      glColor3ubv (window::get_border_color().ubvec);
      for (auto v: this->vertices) {
         glVertex2f (center.xpos + v.xpos, center.ypos + v.ypos);
      }
      glEnd();
   }
}

void shape::show (ostream& out) const {
   out << this << "->" << demangle (*this) << ": ";
}

void text::show (ostream& out) const {
   shape::show (out);
   out << glut_bitmap_font << "(" << fontname[glut_bitmap_font]
       << ") \"" << textdata << "\"";
}

void ellipse::show (ostream& out) const {
   shape::show (out);
   out << "{" << dimension << "}";
}

void polygon::show (ostream& out) const {
   shape::show (out);
   out << "{" << vertices << "}";
}

ostream& operator<< (ostream& out, const shape& obj) {
   obj.show (out);
   return out;
}
